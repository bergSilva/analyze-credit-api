# projeto Analize Crédito

## Requesitos Versão JDK
   - Java 8
  
## Base de Dados
   - MySQL
## Opções de acesso para a API
   Acesso  API Local via postman 
        - https://www.getpostman.com/downloads/
        - http://127.0.0.1:8080/client
## Opções de subir a aplicação 
   - criar a base de dados no MySQl 
        - create database ´analizecreditdb´;
        - seta a variavel no properties
            - spring.jpa.hibernate.ddl-auto = create
        -após subir o sistema, setar novamente para:
            - spring.jpa.hibernate.ddl-auto = validate
         
        
   - Comando MVN 
        - mvn clean spring-boot:run
   - Docke Compose
        - docker-compose up
   