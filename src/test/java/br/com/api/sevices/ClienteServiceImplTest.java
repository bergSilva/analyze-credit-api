package br.com.api.sevices;

import br.com.api.dto.AddressDTO;
import br.com.api.dto.ClientDTO;
import br.com.api.dto.LoanDTO;
import br.com.api.dto.RiskDTO;
import br.com.api.model.Address;
import br.com.api.model.Client;
import br.com.api.repository.ClientRepository;
import br.com.api.service.impl.ClientServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ClienteServiceImplTest {
    @Mock(answer = Answers.RETURNS_SMART_NULLS)
    ClientRepository repository;

    @InjectMocks
    ClientServiceImpl service;

    public Client client = null;
    public Address address = null;
    public ClientDTO clientDTO = null;
    public AddressDTO addressDTO = null;
    public LoanDTO loanDTOClienRiskA = null;
    public LoanDTO loanDTOClienRiskB = null;
    public LoanDTO loanDTOClienRiskC = null;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        clientDTO = new ClientDTO();
        clientDTO.setRedimentoMensal(1000.0);


        loanDTOClienRiskA = new LoanDTO();
        loanDTOClienRiskA.setAmountMonths(12);
        loanDTOClienRiskA.setClientRisk("A");
        loanDTOClienRiskA.setValueLoan(1000.00);

        loanDTOClienRiskB = new LoanDTO();
        loanDTOClienRiskB.setAmountMonths(12);
        loanDTOClienRiskB.setClientRisk("B");
        loanDTOClienRiskB.setValueLoan(1000.00);

        loanDTOClienRiskC = new LoanDTO();
        loanDTOClienRiskC.setAmountMonths(12);
        loanDTOClienRiskC.setClientRisk("C");
        loanDTOClienRiskC.setValueLoan(1000.00);

    }

    @Test
    public void validateCases() {
        RiskDTO casesRisks = service.validateCases(clientDTO.getRedimentoMensal());
        assertEquals("C", casesRisks.getRisk());
    }

    @Test
    public void calculateInterest() {
        Double calculateInterestA = service.calculateInterest(loanDTOClienRiskA);
        assertEquals(19.0, calculateInterestA);
        Double calculateInterestB = service.calculateInterest(loanDTOClienRiskB);
        assertEquals(50.0, calculateInterestB);
        Double calculateInterestC = service.calculateInterest(loanDTOClienRiskC);
        assertEquals(100.0, calculateInterestC);
    }

    @Test
    public void calculeAmount() {
        Double amountA = service.calculeAmount(loanDTOClienRiskA);
        assertEquals(1019.0, amountA);
        Double amountB = service.calculeAmount(loanDTOClienRiskB);
        assertEquals(1050.0, amountB);
        Double amountC = service.calculeAmount(loanDTOClienRiskC);
        assertEquals(1100.0, amountC);
    }

    @Test
    public void loanSimulation() {

        LoanDTO loanDTO = service.loanSimulation(loanDTOClienRiskA);
        assertEquals(1019.0, loanDTO.getAmount());
        assertEquals(19.0, loanDTO.getInterestRate());
        assertEquals(12, loanDTO.getAmountMonths());
        assertEquals("A", loanDTO.getClientRisk());
    }


}
