package br.com.api.service;


import br.com.api.dto.ClientDTO;
import br.com.api.dto.LoanDTO;
import br.com.api.dto.RiskDTO;
import br.com.api.enums.RiskEnum;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public interface ClientService {

   List<ClientDTO> findAll();
   ClientDTO findById(Long id);
   ClientDTO add(ClientDTO dto);
   ClientDTO update(ClientDTO dto);
   ClientDTO createOrUpdate(ClientDTO car);
   void delete(Long id);
   RiskDTO assessrisk(Double value);

   LoanDTO loanSimulation(LoanDTO loanDTO);


}
