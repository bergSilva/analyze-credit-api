package br.com.api.service.impl;

import br.com.api.dto.AddressDTO;
import br.com.api.mapper.AddressMapper;
import br.com.api.model.Address;
import br.com.api.repository.AddressRepository;
import br.com.api.service.AddressService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class AddressServiceImpl implements AddressService {

    @Autowired
    private AddressRepository addressRepository;


    @Override
    public List<AddressDTO> findAll() {
        List<Address> list = (List<Address>) addressRepository.findAll();
        return list.stream().map(this::toAddresDTO).collect(Collectors.toList());
    }


    @Override
    public AddressDTO findById(Long id) {
      Optional<Address> optional= addressRepository.findById(id);
      if (optional.isPresent())
          return new AddressMapper().getAddressDTO(optional.get());
      else
          throw new IllegalArgumentException("Address NotFound");

    }

    @Override
    public AddressDTO add(AddressDTO dto) {
        return this.createOrUpdate(dto);
    }

    @Override
    public AddressDTO update(AddressDTO dto) {
        return this.createOrUpdate(dto);
    }

    @Override
    public AddressDTO createOrUpdate(AddressDTO dto) {
       return new AddressMapper().getAddressDTO(addressRepository.save(new AddressMapper().getAddressEntity(dto)));
    }

    @Override
    public void delete(Long id) {
         Address address=  new AddressMapper().getAddressEntity(this.findById(id));
         addressRepository.delete(address);
    }

    private AddressDTO toAddresDTO(Address address) {
    return  new AddressMapper().getAddressDTO(address);
    }


}
