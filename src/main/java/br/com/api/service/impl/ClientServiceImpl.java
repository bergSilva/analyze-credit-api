package br.com.api.service.impl;

import br.com.api.dto.ClientDTO;
import br.com.api.dto.LoanDTO;
import br.com.api.dto.RiskDTO;
import br.com.api.enums.RiskEnum;
import br.com.api.mapper.ClientMapper;
import br.com.api.model.Client;
import br.com.api.repository.ClientRepository;
import br.com.api.service.ClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository clientRepository;


    @Override
    public List<ClientDTO> findAll() {
        List<Client> list = (List<Client>) clientRepository.findAll();
        return list.stream().map(this::toClientDTO).collect(Collectors.toList());
    }


    @Override
    public ClientDTO findById(Long id) {
        Optional<Client> optional = clientRepository.findById(id);
        if (optional.isPresent())
            return new ClientMapper().toClientDTO(optional.get());
        else
            throw new IllegalArgumentException("Cliente Noto Fout");

    }

    @Override
    public ClientDTO add(ClientDTO dto) {
        return this.createOrUpdate(dto);
    }

    @Override
    public ClientDTO update(ClientDTO dto) {
        return this.createOrUpdate(dto);
    }

    @Override
    public ClientDTO createOrUpdate(ClientDTO dto) {
        return new ClientMapper().
                toClientDTO(clientRepository.save(new ClientMapper().toClientEntity(dto)));
    }

    @Override
    public void delete(Long id) {
        Client client = new ClientMapper().toClientEntity(this.findById(id));
        clientRepository.delete(client);
    }

    @Override
    public RiskDTO assessrisk(Double value) {

        return validateCases(value);
    }

    @Override
    public LoanDTO loanSimulation(LoanDTO loanDTO) {
        return LoanDTO.builder()
                .interestRate(calculateInterest(loanDTO))
                .amount(calculeAmount(loanDTO))
                .clientRisk(loanDTO.getClientRisk())
                .amountMonths(loanDTO.getAmountMonths())
                .valueLoan(loanDTO.getValueLoan())
                .build();
    }

    public Double calculeAmount(LoanDTO loanDTO) {
        return loanDTO.getValueLoan() + calculateInterest(loanDTO);
    }

    public Double calculateInterest(LoanDTO loanDTO) {
        switch (loanDTO.getClientRisk()) {
            case "A":
                return (loanDTO.getValueLoan() * 1.9) / 100;
            case "B":
                return (loanDTO.getValueLoan() * 5) / 100;
            case "C":
                return (loanDTO.getValueLoan() * 10) / 100;
            default:
                return 0.0;

        }
    }

    public ClientDTO toClientDTO(Client client) {
        return new ClientMapper().toClientDTO(client);
    }

    public RiskDTO validateCases(Double value) {

        if (value <= 2000) {
            return RiskDTO.builder().risk(RiskEnum.C.getDescricao()).build();
        } else if ((value > 2000) && (value <= 8000))
            return RiskDTO.builder().risk( RiskEnum.B.getDescricao()).build();
        else if (value > 8000)
            return  RiskDTO.builder().risk(RiskEnum.A.getDescricao()).build();
        else

            return RiskDTO.builder().risk("Opicão Iválida").build();
    }


}
