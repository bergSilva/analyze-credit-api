package br.com.api.service;


import br.com.api.dto.AddressDTO;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public interface AddressService {

   List<AddressDTO> findAll();
   AddressDTO findById(Long id);
   AddressDTO add(AddressDTO dto);
   AddressDTO update(AddressDTO dto);
   AddressDTO createOrUpdate(AddressDTO dto);
   void delete(Long id);


}
