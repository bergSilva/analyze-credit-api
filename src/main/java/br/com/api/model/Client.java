package br.com.api.model;

import br.com.api.enums.RiskEnum;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
@Getter
@Setter
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "client")
public class Client implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cli_id")
    private Long id;
    private String name;
    @Column(name = "monthlysalary")
    private Double monthlySalary;
    @Column(name = "risck")
    @Enumerated(EnumType.STRING)
    private RiskEnum riskEnum;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "client_address",
            joinColumns = @JoinColumn(name = "cli_id"),
            inverseJoinColumns = @JoinColumn(name = "add_id"))
    private List<Address> addressList = new ArrayList<>();


}
