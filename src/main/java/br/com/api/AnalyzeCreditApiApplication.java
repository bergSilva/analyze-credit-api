package br.com.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnalyzeCreditApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnalyzeCreditApiApplication.class, args);
	}

}
