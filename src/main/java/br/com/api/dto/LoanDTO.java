package br.com.api.dto;

import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LoanDTO {
    private String clientRisk;
    private Double valueLoan;
    private Integer amountMonths;
    private Double interestRate;
    private Double amount;
}
