package br.com.api.dto;

import lombok.*;

import java.io.Serializable;


@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AddressDTO implements Serializable {

    private Long id;
    private String street;
    private String number;
    private String zipCode;

}
