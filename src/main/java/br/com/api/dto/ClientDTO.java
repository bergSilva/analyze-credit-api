package br.com.api.dto;

import br.com.api.enums.RiskEnum;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


@Getter
@Builder
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ClientDTO implements Serializable {

    private Long id;

    private String name;

    private Double redimentoMensal;

    private RiskEnum riskEnum;

    private List<AddressDTO> address = new ArrayList<>();

}
