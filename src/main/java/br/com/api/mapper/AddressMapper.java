package br.com.api.mapper;

import br.com.api.dto.AddressDTO;
import br.com.api.model.Address;

import java.util.List;
import java.util.stream.Collectors;

public class AddressMapper {
    public Address getAddressEntity(AddressDTO dto) {
        return  Address.builder()
                .id(dto.getId())
                .number(dto.getNumber())
                .street(dto.getStreet())
                .zipCode(dto.getZipCode())
                .build();
    }

    public AddressDTO getAddressDTO(Address entity) {
        return  AddressDTO.builder()
                .id(entity.getId())
                .number(entity.getNumber())
                .street(entity.getStreet())
                .zipCode(entity.getZipCode())
                .build();
    }

    public List<AddressDTO> getListAddressDTO(List<Address> list) {
        return  list.stream().map(this::getAddressDTO).collect(Collectors.toList());
    }

    public List<Address> getListAddressEntity(List<AddressDTO> list) {
        return  list.stream().map(this::getAddressEntity).collect(Collectors.toList());
    }


}
