package br.com.api.mapper;

import br.com.api.dto.ClientDTO;
import br.com.api.model.Client;

public class ClientMapper {

    public ClientDTO toClientDTO(Client client) {
        return ClientDTO.builder()
                .id(client.getId())
                .name(client.getName())
                .redimentoMensal(client.getMonthlySalary())
                .riskEnum(client.getRiskEnum())
                .address(new AddressMapper().getListAddressDTO(client.getAddressList()))
                .build();
    }

    public Client toClientEntity(ClientDTO dto) {
        return Client.builder()
                .id(dto.getId())
                .name(dto.getName())
                .monthlySalary(dto.getRedimentoMensal())
                .riskEnum(dto.getRiskEnum())
                .addressList(new AddressMapper().getListAddressEntity(dto.getAddress()))
                .build();
    }


}
