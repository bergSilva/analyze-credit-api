package br.com.api.controller;

import br.com.api.dto.ClientDTO;
import br.com.api.dto.LoanDTO;
import br.com.api.dto.RiskDTO;
import br.com.api.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController()
@RequestMapping("/client")
@CrossOrigin
public class ClientController {

    @Autowired
    private ClientService service;

    @GetMapping
    public List<ClientDTO> findAll(){
        return  service.findAll();
    }

    @GetMapping("/{id}")
    public ClientDTO findById(@PathVariable Long id){
        return  service.findById(id);
    }

    @PostMapping
    public ClientDTO create(@RequestBody @Valid ClientDTO dto){
        return  service.add(dto);
    }

    @PutMapping("/{id}")
    public ClientDTO updateClient(@RequestBody @Valid ClientDTO dto){
        return  service.update(dto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete( @PathVariable("id") Long id){
        service.delete(id);
    }

    @GetMapping("/assessrisk/{value}")
    public RiskDTO assessrisk(@PathVariable("value") Double value){
        return  service.assessrisk(value);
    }

    @PostMapping("/simulation")
    public LoanDTO  loanSimulation(@RequestBody LoanDTO dto){
        return  service.loanSimulation(dto);
    }


}
