package br.com.api.enums;

public enum RiskEnum {
    A("A"),
    B("B"),
    C("C");

    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    RiskEnum(String descricao) {
        this.descricao = descricao;
    }
}
