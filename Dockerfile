FROM openjdk:8 AS Builder
RUN apt-get update
RUN apt-get install -y maven
COPY pom.xml /app/pom.xml
COPY src /app/src
WORKDIR /app
RUN mvn clean package

FROM openjdk:8
WORKDIR /app
COPY --from=Builder /app/target/challenge.jar .
#ADD target/challenge.jar .
ENTRYPOINT ["java", "-jar", "challenge.jar"]